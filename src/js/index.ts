import { OUTAGE, RELAY_URL, SITE_NAME, SUBDOMAIN } from "./constants";

if (OUTAGE) {
  // force-redirect users to a page telling them that the service is down
  window.location.replace("outage/index.html");
}

if (
  document.cookie.includes("id") &&
  !document.cookie.includes("id=NONE") &&
  document.cookie.includes("first") &&
  !document.cookie.includes("first=NONE")
) {
  // Cookies have already been set; already logged in
  window.location.replace("loading/index.html");
}

window.onload = () => {
  document.getElementById("site-name").innerHTML = SITE_NAME;

  // Will execute upon hitting the login button
  document.querySelector(".btn-submit").addEventListener("click", () => {
    // Let the user know that we're loading
    (document.querySelector(".btn-submit") as HTMLButtonElement).innerText =
      "Please wait...";

    // Get the info from the two inputs
    const id: number = parseInt(
      (document.getElementById("id-num") as HTMLInputElement).value,
      10,
    );
    const first: string = (document.getElementById(
      "first-name",
    ) as HTMLInputElement).value;

    // Verify user identity with the API
    fetch(`${RELAY_URL}?get=login&id=${id}&first=${first}&subdomain=${SUBDOMAIN}`).then((resp) => {
      resp.json().then((val: boolean) => {
        if (val) {
          // Information provided is valid; store as cookies and load the next page
          document.cookie = `id=${id};path=/`;
          document.cookie = `first=${first};path=/`;
          window.location.replace("loading/index.html");
        } else {
          alert(
            "Login incorrect. Please try again or email milo.gilad@mynbps.org for assistance.",
          );
          (document.querySelector(
            ".btn-submit",
          ) as HTMLButtonElement).innerText = "Try Again?";
        }
      });
    });
  });
};
